repository url: `https://gitlab.com/user/my-project`

### Requirements
- [ ] I have read and accepted the [Terms and Conditions](https://py.wiki/toc).
- [ ] I acknowledge that as the owner of the website, I have all rights and responsibility for the content of the website.
- In case of python library, the source code is published under a [FLOSS license](https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses).
- In case of wiki, documentation, tutorial or guide, the content is published under a [creative commons](https://creativecommons.org/)

