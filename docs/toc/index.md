---
hide:
  - navigation
---
# Terms and Conditions


## 1 Principles

1. PY.WIKI is a free service provided by the owner of PY.WIKI. The service will be only called "PY.WIKI". All services are subject to these terms and conditions.
1. PY.WIKI provides free subdomains under the domain "py.wiki" to be used as a custom URL for any static web hosting.
1. Communication between PY.WIKI and the user will take place on GitLab.com in the form of issues, pull requests or comments.
1. By submitting a pull request in the GitLab.com repository that hosts PY.WIKI with the wish to add a record to the list of domains, the user attends the PY.WIKI service and accepts the terms and conditions as the sole basis of all services provided by PY.WIKI.

## 2 Availability and reservations

PY.WIKI aims to ensure the constant availability of the subdomain service. A right on constant availability does not exist. All services in connection with this free service will be provided on a voluntary basis. The user acquires no ownership or other rights to single services, the registered subdomains and the whole service.

If the user violates the terms, PY.WIKI is entitled to exclude the user from the service without notice subjected to damages and other claims. PY.WIKI is entitled to terminate or modify the service at any time without notice and without explanation. In that case the user of PY.WIKI will be informed timely, at least 7 days in advance by PY.WIKI. The relevant information about the impending use and deletion is shown simultaneously to the concerned users.

## 3 Termination

1. Both sides can terminate at any time without stating any reasons.
1. The user cancels the service by removing the subdomain from their web hosting service and making a pull request in the repository of PY.WIKI with the wish to remove their subdomain from the list of active PY.WIKI domains.
1. PY.WIKI may also terminate the provision of a certain or all subdomains. Concerned users will be notified at least 7 days in advance by an issue in their git repository.

## 4 Rights and Duties of the user

A transfer of a PY.WIKI subdomain is allowed as long as the new users accepts these terms and conditions. The user receives a non-exclusive right from PY.WIKI to use the service. The use permit expires with cancellation of the conctractual agreement.

The user is fully responsible for the content provided by their static web hosting, and is also responsible to extricate PY.WIKI from all third party claims. The user warrants not to transmit any material that violates the rights of others or violates statutory provisions.

In particular providing content like the following is prohibited:

* Piracy (warez, videos, MP3s, DVDs, software, etc.)
* Phishing, scam, malware, viruses
* Copyright offense
* Dubious business models (eg, pyramid schemes, chain letters, etc.)
* Content that is unlawful, obscene, threatening, abusive, libelous, or scandalous
* Content which led to committing criminal acts or acts that undermine the civil order, or otherwise contrary to national or international laws
* Pornography or content intended for adults only
* Unsecured Web 2.0 platforms (eg. no captcha protection in forums, blogs, guest books)
* File or Image hosting
* Sites with other illegal content

A violation of these rules will result in immediate deletion of the PY.WIKI domain.


## 5 Limitation of Liability

* In principle PY.WIKI takes no responsability for the support. The use of the provided services of PY.WIKI takes place at your own risk. This refers in particular to the functionality, availability and freedom from viruses of the provided services as well as damage caused by the use of programs, program components and scripts/applets. The user is obliged to create local copies of all his data at PY.WIKI for security reasons and regularly to secure all stored data. PY.WIKI indicates that PY.WIKI is a free service and does not create data backups for the users.
* The user is liable for any use of the service performed by their web hosting account. Legal representatives are liable for minors. The user therefore has to ensure that his access can not be misused by third parties.
* PY.WIKI provides the service without any express or implied warranties. PY.WIKI is not liable for inadvertent disclosure, damage or loss of transmitted, received or stored data from PY.WIKI. The user expressly acknowledges that these terms also effect third party supplies.
* For the purposed service usage as described above PY.WIKI solely assume the liability for the injury to life, limb and health, and other damages caused by gross neglect of duty on PY.WIKI.
* A liability of PY.WIKI for slight negligence exists only for breach of an essential contractual obligation.
* A liability of PY.WIKI for further damages, in particular consequential damages, indirect damages or lost profits is excluded.
* A liability for malfunctions or failure as well as unauthorized access from outside the sphere of influence of PY.WIKI is only in the context of intent and gross negligence.

## 6 Name of the subdomain

Each user can select the names of subdomains with respect to a public available list of reserved names published in the PY.WIKI repository. It is sole responsibility of the user to consider whether the name is contrary to statutory provisions, third party rights (including trade marks or registered rights), or morality. If this is the case, the registration of the subdomain is to be omitted. The same applies to subdomains, whose name is misleading, or suspect websites which are excluded of the participation in the service because of their content according to 4.

## 7 Absolute ban of spam

References to domains registered at PY.WIKI in unsolicited commercial e-mail or similar media (spam) are not allowed. The mass registering of domains for the purpose of irritation to the users of search engines are also considered spam and are also inadmissible.

## 8 Manipulation

It is prohibited to auto retrieve masses of PY.WIKI subdomains.

## 9 Responsibility

PY.WIKI is not responsible for the names of subdomains or the content of external websites that are registered as link destinations of subdomains, or otherwise linked to this service.

## 10 Place of jurisdiction

Service provision and the usage of the service according to these terms and conditions is exclusively subject to the application of the Swiss law. Application of conflict of laws of private international law is expressly excluded.

The place of jurisdiction for PY.WIKI is Zürich in Switzerland.

For disputes with users who are defined as contractors, Zürich is agreed as place of jurisdiction. For users who do not have general jurisdiction and no resident in Switzerland at the time of the commencement of proceedings, Zürich will be the place of jurisdiction.

For users, who are consumers within the meaning. The general jurisdiction of the domicile will remain in Zürich.

## 11 Change of Terms

PY.WIKI reserves the right to make changes to the Terms of Service. The user will be informed by PY.WIKI of the changes through a notice in the readme section of the PY.WIKI repository.

## 12 Severability clause

If single clauses of these terms and conditions are fully or partially invalid or void, respectively will be, so otherwise the validity of these terms and conditions remains untouched.

<br/>
Last updated: **May 29, 2022**
