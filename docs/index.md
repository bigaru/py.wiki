---
hide:
  - navigation
  - toc
---
# Home

## What is py.wiki
Inspired by [js.org](https://js.org) project, we provide a free and stylish subdomains for python-related projects

<span style="border-radius: 5px; border: 1px solid #000; padding: 0.4em 0.8em; font-size: 1.4em;">https://<span style="color:var(--md-primary-fg-color--dark)">your-project</span>.py.wiki</span>

## Quickstart

1. Publish your content on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/), [GitHub Pages](https://docs.github.com/en/pages) or any static web hosting.
1. Choose a suitable subdomain for your project such as `my-project.py.wiki`
1. Add the chosen subdomain to your page
1. Merge-request process
    * Fork [our repository](https://gitlab.com/bigaru/py.wiki)
    * Add your subdomain under `dns_config.yml`
    * Open a merge-request


## Similiar projects
* [js.org](https://github.com/js-org/js.org) - subdomains for js projects
* [mod.land](https://github.com/denosaurs/mod.land) - subdomains for deno projects
* [is-a.dev](https://github.com/is-a-dev/register) - subdomain for for developers personal website
