#!/usr/bin/env python

import CloudFlare
import os
from ruamel.yaml import YAML

zone_id = '9bde94a766aa158938b5d60324b889a1' # for py.wiki
config_file = 'dns_config.yml'

def load_records(record_type):
    with open(config_file, 'r') as file:
        yaml=YAML(typ='safe')   
        config = yaml.load(file)

        items = {k.lower():v for k,v in config[record_type].items()}

    for reserved in config['RESERVED']:
        if reserved.lower() + '.py.wiki' in items:
            raise Exception('Using reserved word')

    return config[record_type]


def prepare_records(config, record_type):
    prepared_records = list()
    default_proxied = False if record_type == 'TXT' else True
    
    for k,v in config.items():
        if isinstance(v, str):
            prepared_records.append({'name': k, 'content':v, 'ttl': 1, 'type': record_type, 'proxied': default_proxied})
        else:
            prepared_records.append({'name': k, 'content':v['name'], 'ttl': 1, 'type': record_type, 'proxied': v.get('proxied', default_proxied)})
    return prepared_records

def put_records(new_records, record_type):
    cf = CloudFlare.CloudFlare(token=os.environ['API_TOKEN'])

    dns_records = cf.zones.dns_records.get(zone_id, params={'type': record_type})
    existing_records_dict = {r['name']:r['id'] for r in dns_records}

    for record in new_records:
        full_name = record['name'] 

        if full_name in existing_records_dict:
            cname_id = existing_records_dict[full_name]
            result = cf.zones.dns_records.put(zone_id, cname_id, data=record)
        else:
            result = cf.zones.dns_records.post(zone_id, data=record)


if __name__ == '__main__':
    loaded_cnames = load_records('CNAME')
    prepared_cnames = prepare_records(loaded_cnames, 'CNAME')
    put_records(prepared_cnames, 'CNAME')    
    
    loaded_txts = load_records('TXT')
    prepared_txts = prepare_records(loaded_txts, 'TXT')
    put_records(prepared_txts, 'TXT')
